<?php
    function Soma($x, $y){
        
        $z = $x + $y;
        
        if(!VeriSoma($x, $y, $z)){
            return "Soma Invalida!";
        }
        
        return $z;
    }

    function Produto($x, $y){

        $z = $x * $y;
        
        if(!VeriProduto($x, $y, $z)){
            return "Produto Invalido!";
        }

        return $z;
    }

    function VeriSoma($x, $y, $z){
        if(
            ($x + $y) + $z == $x + ($y + $z) &&
            $x + $y + $z == $z + $y + $x &&
            $x + 0 == $x &&
            $y + 0 == $y &&
            $z + 0 == $z &&
            $x - $x == 0 &&
            $y - $y == 0 &&
            $z - $z == 0 
        ){
            return true;
        }
        return false;
    }

    function VeriProduto($x, $y, $z){
        if(
            ($x * $y) * $z == $x * ($y * $z) &&
            $x * $y * $z == $z * $y * $x &&
            $x * 1 == $x &&
            $y * 1 == $y &&
            $z * 1 == $z &&
            $x * (1/$x) == 1 &&
            $y * (1/$y) == 1 &&
            $z * (1/$z) == 1 
        ){
            return true;
        }
        return false;
    }

    echo(Soma(2,2));
    echo(Produto(2,2));
?>